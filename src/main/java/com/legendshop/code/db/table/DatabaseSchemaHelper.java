package com.legendshop.code.db.table;

import java.util.List;


import com.legendshop.code.config.Table;
import com.legendshop.code.db.Column;

public interface DatabaseSchemaHelper {
	public List<Column> getTableColumns(String database,String tableName);
	
	public Table getTableSimpleDefinition(String database,String tableName);
}
