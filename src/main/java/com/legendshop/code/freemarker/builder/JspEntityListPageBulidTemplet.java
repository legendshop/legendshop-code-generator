package com.legendshop.code.freemarker.builder;

import java.util.Map;


import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.StringConverter;
import com.legendshop.code.util.Templet;

@Templet
public class JspEntityListPageBulidTemplet extends AbstractBulidTemplet {

	public JspEntityListPageBulidTemplet(){
		
	}
	
	
	@Override
	public boolean buildSourceFile(String tableName) {
		//System.out.println("Start Building Jsp Entity List Page.");
		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.ROOT + PackageConfigUtil.getJspPath();
		String templateFileName = "JspEntityListPage.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);
		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("columns", table.getColumnList());
		paramMap.put("table", table);
		
		String entityClass = (String) paramMap.get("entityClass");
		String upcapedEntityClass =  StringConverter.setFirstCharLowercase(entityClass);
		outputDir = outputDir + "/" + upcapedEntityClass;

		String fileName = upcapedEntityClass + "List.jsp";

		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,false);

		//System.out.println("End Build Jsp Entity List Page.\n");
		return result;
	}

}
