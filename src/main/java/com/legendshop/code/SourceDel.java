package com.legendshop.code;


import java.io.File;

import com.legendshop.code.config.Config;
import com.legendshop.code.util.FileProcessor;

public class SourceDel {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Config config = Config.getInstance();
        //		System.out.println("DataBaseType = "+config.getDataBaseType());
        //		System.out.println("DbConnectString = "+config.getDbConnectString());
        //		System.out.println("getEntityName = "+config.getEntityName());
        //		for (int i=0;i<config.getTableList().length;i++){
        //			System.out.println("getTableList = "+i+"  "+config.getTableList()[i]);
        //		}
        System.out.println("Begin to delete Java Command Framework source!");
        
       String path = getBaseDir() + config.getPath() + "/src";
        System.out.println("Source folder = " + path);
        FileProcessor.deleteDirectory(new File(path));
    }
    
	protected static String getBaseDir() {
		String usrDir = System.getProperty("user.dir");
		if (usrDir.endsWith("/lib")) {
			usrDir = usrDir.substring(0, usrDir.length() - 5);
		}
		return usrDir;
	}

}
