package ${modelPackagePath} ;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *${table.tableComment}
 */
@Entity
@Table(name = "${tableName}")
public class ${entityClass} implements GenericEntity<Long> {

<#list columns as column>
<#if column.columnComment??>
	/** ${column.columnComment} */
</#if> 
	private ${column.javaSimpleType} ${column.javaName}; 
		
</#list>
	
	public ${entityClass}() {
    }
<#list columns as column>
		
<#if !column.isPrimaryKeyColumn()>
    @Column(name = "${column.columnName}")
<#else> 
	@Id
	@Column(name = "${column.columnName}")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "${sequenceName}")
</#if> 
	public ${column.javaSimpleType}  get<@upperFirstChar>${column.javaName}</@upperFirstChar>(){
		return ${column.javaName};
	} 
		
	public void set<@upperFirstChar>${column.javaName}</@upperFirstChar>(${column.javaSimpleType} ${column.javaName}){
			this.${column.javaName} = ${column.javaName};
		}
	</#list>
	
<#list columns as column>	
<#if column.isPrimaryKeyColumn()>
<#if column.javaName != "id">
	@Transient
	public ${IdType} getId() {
		return ${entityId};
	}
	
	public void setId(${IdType} id) {
		${entityId} = id;
	}
</#if> 
</#if> 
</#list>


} 
