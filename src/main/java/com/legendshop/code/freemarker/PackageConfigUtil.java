package com.legendshop.code.freemarker;

import com.legendshop.code.config.Config;

public class PackageConfigUtil {

	public static String getModelPackagePath() {
		String base = Config.getInstance().getModelPackagePath();
		return replace(base);
	}

	public static String getDaoPackagePath() {
		String base = Config.getInstance().getDaoPackagePath();
		return replace(base);
	}

	public static String getDaoImplPackagePath() {
		String base = Config.getInstance().getDaoPackagePath() + ".impl";
		return replace(base);
	}

	public static String getControllerPackagePath() {
		String base = Config.getInstance().getControllerPackagePath();
		return replace(base);
	}

	public static String getServicePackagePath() {
		String base = Config.getInstance().getServicePackagePath();
		return replace(base);
	}

	public static String getServiceImplPackagePath() {
		String base = Config.getInstance().getServicePackagePath() + ".impl";
		return replace(base);
	}
	
	public static String getSpringConfigPackagePath() {
		String base = Config.getInstance().getXmlConfigPath();
		return replace(base);
	}
	
	public static String getJspPath() {
		String base = Config.getInstance().getJspPath();
		return replace(base);
	}

	private static String replace(String str, boolean isPath) {
		if (isPath) {
			return "/" + str.replace(".", "/");
		} else {
			return str;
		}
	}

	private static String replace(String str) {
		return "/" + str.replace(".", "/");
	}

}
