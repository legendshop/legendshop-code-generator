package com.legendshop.code.test.util;

import org.apache.oro.text.regex.MalformedPatternException;


public class StringTest {
	public static void main(String[] args) throws MalformedPatternException {
		String[] values={"1","2","3","4","5"};
		String text="insert into table values (?,?,?,?,?)";
		String result =convert(text, values);
		System.out.println(result);

	}
	
	public static String convert(String text,String[]values){
		StringBuffer textBuf = new StringBuffer(text);
		int offset = 0;
		while(textBuf.indexOf("?")!=-1){
			textBuf.replace(textBuf.indexOf("?"), textBuf.indexOf("?")+1, values[offset]);
			offset++;
			if(offset>values.length-1) break;
			
		}
		return textBuf.toString();
	}
}
