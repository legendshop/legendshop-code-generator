package com.legendshop.code.sequence;

public class Sequence {
	private String sequenceName;
	private Long nextVal;
	public String getSequenceName() {
		return sequenceName;
	}
	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}
	public Long getNextVal() {
		return nextVal;
	}
	public void setNextVal(Long nextVal) {
		this.nextVal = nextVal;
	}
}
