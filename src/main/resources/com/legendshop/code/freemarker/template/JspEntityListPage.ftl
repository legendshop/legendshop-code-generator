<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
    <meta charset="utf-8">
    <title>热门商品 - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="${table.tableComment}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="${r"${"}contextPath}/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" href="${r"${"}contextPath}/favicon.ico">
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x: auto;">
        <table class="${r"${"}tableclass} title-border" style="width: 100%">
            <tr>
                <th class="title-border">首页管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"><a href="${r"${"}contextPath}${actionPrex}/${entityClassInstance}/query">${table.tableComment}</a></span></th>
            </tr>
        </table>
        <form:form action="${r"${"}contextPath}${actionPrex}/${entityClassInstance}/query" id="form1" method="post">
            <div class="criteria-div">
						<span class="item">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${r"${"}curPageNO}" />
							示例查询条件：<input class="${r"${"}inputclass}" type="text" name="title" maxlength="50" value="${r"${"}bean.title}" size="30" />
							<input class="${r"${"}btnclass}" type="button" onclick="search()" value="搜索" />
							<input class="${r"${"}btnclass}" type="button" value="创建${table.tableComment}" onclick='window.location="<ls:url address='${actionPrex}/${entityClassInstance}/load'/>"' />
						</span>
            </div>
        </form:form>
        <div align="center" class="order-content-list">
            <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
            <display:table name="list" requestURI="${r"${"}contextPath}${actionPrex}/${entityClassInstance}/query" id="item" export="false" class="${r"${"}tableclass}" style="width:100%" sort="external">
        	    <#list columns as column>
         		  <display:column title="<@upperFirstChar>${column.javaName}</@upperFirstChar>" property="${column.javaName}"></display:column>
    		    </#list>
                <display:column title="操作" media="html" style="width:300px">
                    <div class="table-btn-group">
                        <button class="tab-btn" onclick="window.location='${r"${"}contextPath}${actionPrex}/${entityClassInstance}/load/${r"${"}item.id}'">修改</button>
                        <span class="btn-line">|</span>
                        <button class="tab-btn" onclick="deleteById('${r"${"}item.id}');">删除</button>
                    </div>
                </display:column>
            </display:table>
            <div class="clearfix">
                <div class="fr">
                    <div class="page">
                        <div class="p-wrap">
                            <ls:page pageSize="${r"${"}pageSize}"  total="${r"${"}total}" curPageNO="${r"${"}curPageNO}"  type="simple"/>
                        </div>
                    </div>
                </div>
            </div>
            <table style="width: 100%; border: 0px; margin-top: 10px;" class="${r"${"}tableclass}">
                <tr>
                    <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 
                    1、 xxxxxxxx<br>
                    2、 xxxxxxxx。
                    </td>
                <tr>
            </table>
        </div>
    </div>
</div>
</body>

<script language="JavaScript" type="text/javascript">
    $(document).ready(function() {

    });

    function deleteById(id) {
        layer.confirm('确定给删除？', {icon:3, title:'提示'}, function(){
            window.location = "${r"${"}contextPath}/admin/${entityClassInstance}/delete/" + id;
        });
    }

    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>