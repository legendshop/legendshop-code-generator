package com.legendshop.code.freemarker.builder;

import java.util.Map;

import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.Templet;

@Templet
public class MapperBulidTemplet extends AbstractBulidTemplet {

	public MapperBulidTemplet(){
		
	}
	
	@Override
	public boolean buildSourceFile(String tableName) {
		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.MAIN_PATH + PackageConfigUtil.getModelPackagePath();
		String templateFileName = "Mapper.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);
		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("columns", table.getColumnList());
		paramMap.put("table", table);
		String entityClass = (String) paramMap.get("entityClass");
		String fileName = entityClass + "Mapper.java";

		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,false);

		return result;
	}
	
	
}
