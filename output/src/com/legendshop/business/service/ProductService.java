/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Product;

/**
 * The Class ProductService.
 * 商品服务接口
 */
public interface ProductService  {

   	/**
	 *  根据Id获取商品
	 */
    public Product getProduct(Long id);

    /**
	 *  根据Id删除商品
	 */
    public int deleteProduct(Long id);
    
    /**
	 *  根据对象删除商品
	 */
    public int deleteProduct(Product product);
    
   /**
	 *  保存商品
	 */	    
    public Long saveProduct(Product product);

   /**
	 *  更新商品
	 */	
    public void updateProduct(Product product);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<Product> queryProduct(String curPageNO,Integer pageSize);
    
}
