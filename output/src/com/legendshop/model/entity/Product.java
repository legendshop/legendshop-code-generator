package com.legendshop.model.entity ;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *商品
 */
@Entity
@Table(name = "ls_prod")
public class Product implements GenericEntity<Long> {

	/** 产品ID */
	private Long prodId; 
		
	/** 产品版本 */
	private Long version; 
		
	/** 线上渠道 */
	private Boolean channelShop; 
		
	/** 线下渠道 */
	private Boolean channelStore; 
		
	/** 商品一级分类 */
	private Long firstCatId; 
		
	/** 商品二级分类 */
	private Long secondCatId; 
		
	/** 商品三级分类 */
	private Long thirdCatId; 
		
	/** 计量单位Id */
	private String unitId; 
		
	/** 计量单位 */
	private String unit; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商品名字 */
	private String name; 
		
	/** 销售价格 */
	private java.math.BigDecimal price; 
		
	/** 成本价格 */
	private java.math.BigDecimal costprice; 
		
	/** 市场价格(划线价) */
	private java.math.BigDecimal marketprice; 
		
	/** 活动标签 */
	private String description; 
		
	/** 商品卖点 */
	private String brief; 
		
	/** 已经销售数量 */
	private Long buys; 
		
	/** 评论数 */
	private Long comments; 
		
	/** 商品图片 */
	private String pic; 
		
	/** 图片列表 */
	private String images; 
		
	/** 产品状态， 1: 上架，0: 下架， 2：被平台违规下架， -1：商品被逻辑删除， 回收站的商品； 参见ProductStatusEnum枚举 */
	private Long status; 
		
	/** 销售库存 */
	private Long stocks; 
		
	/** 实际库存 */
	private Long actualStocks; 
		
	/** 库存警告 */
	private Long stocksArm; 
		
	/** 商品类型，P.普通商品，G:团购商品,参见ProductTypeEnum */
	private String prodType; 
		
	/** 虚拟商品类型，参见VisualProductTypeEnum */
	private String visualProdType; 
		
	/** 商品动态参数 */
	private String parameter; 
		
	/** 品牌 */
	private Long brandId; 
		
	/** 评论得分 */
	private Long reviewScores; 
		
	/** 是否支持分销 */
	private Boolean supportDist; 
		
	/** 佣金设置选择  0默认  1自定义 */
	private Long commissionRateSettingSelect; 
		
	/** 会员直接上级分佣比例 */
	private Double firstLevelRate; 
		
	/** 会员上二级分佣比例 */
	private Double secondLevelRate; 
		
	/** 会员上三级分佣比例 */
	private Double thirdLevelRate; 
		
	/** 门店Id */
	private Long storeId; 
		
	/** 门店分类Id */
	private Long storeCategory; 
		
	/** 导购Id，导购上传的商品 */
	private Long guideId; 
		
	/** 手机端商品详情 */
	private String contentM; 
		
	/** PC端商品详情 */
	private String content; 
		
	/** 录入时间 */
	private Date recDate; 
		
	/** 修改时间 */
	private Date modifyDate; 
		
	/** 商品的微信小程序码 */
	private String wxACode; 
		
	/** 审核意见 */
	private String auditOpinion; 
		
	/** 商品类型  GoodsTypeEnum  normal：普通  overseas：海淘 */
	private String goodsType; 
		
	/** 关联的海关信息ID */
	private Long customsMessageId; 
		
	/** 运费设置类型   FreightTypeEnum  0:商家承担运费 1：使用商家运费规则 2：固定运费 */
	private Long freightType; 
		
	/** 运费规则ID  当freightType为1才有值 */
	private Long transportId; 
		
	/** 固定的运费值 当freightType为2才有值 */
	private java.math.BigDecimal settledFreight; 
		
	/** 进口税类型,CustomDutyTypeEnum   DUTY_INCLUDE：商品含税，EXTRA_INCLUDE：独立计算税额 */
	private String customDutyType; 
		
	/** 跨境模式,CrossBorderTypeEnum  BONDED_WAREHOUSE：保税仓备货, DIRECT_MAIL: 海外直邮 */
	private String crossBorderType; 
		
	/** 是否开启质保卡 */
	private Boolean enableWarrantyCard; 
		
	/** 质保卡有效时长, 单位是天 */
	private Long warrantyCardEffectiveTime; 
		
	/** 供应商商品id */
	private Long supplierSpuId; 
		
	/** 供应商商品来源：SELF 自有, INNER 内部供应商, JD 京东, SUNING 苏宁  CARDCOUPON (卡券) */
	private String prodSource; 
		
	/** 供应商商品价格上浮类型 */
	private String floatType; 
		
	/** 供应商商品价格上浮值 */
	private java.math.BigDecimal floatValue; 
		
	/** 是否显示供应商信息 */
	private Boolean displaySupplierInformation; 
		
	/** 配送方式 ProductDeliveryTypeEnum */
	private String deliveryType; 
		
	/** 是否参加 员工奖 */
	private Boolean supportEmployeeReward; 
		
	/** 税费开关 */
	private Boolean taxesSwitch; 
		
	/** 失效状态, 0: 正常商品， 10：已失效 */
	private Long invalidStatus; 
		
	/** 移动端详情图片集合 */
	private String contentImages; 
		
	/** 是否强制上架 */
	private Boolean forceOnline; 
		
	/** 是否参加分销团队奖 */
	private Boolean supportTeamAward; 
		
	/** 是否参加业绩奖 */
	private Boolean supportPerformanceAward; 
		
	/** 商品视频url */
	private String video; 
		
	/** 分销市场ID */
	private Long marketId; 
		
	/** 供应商ID */
	private Long supplierId; 
		
	/** 二级供应商ID */
	private Long secondSupplierId; 
		
	/** 商品拓展信息 */
	private String jsonInfo; 
		
	/** 商品在分销市场的状态，1：正常，0：异常 */
	private Long salesMarketProductStatus; 
		
	
	public Product() {
    }
		
	@Id
	@Column(name = "prod_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_SEQ")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "version")
	public Long  getVersion(){
		return version;
	} 
		
	public void setVersion(Long version){
			this.version = version;
		}
		
    @Column(name = "channel_shop")
	public Boolean  getChannelShop(){
		return channelShop;
	} 
		
	public void setChannelShop(Boolean channelShop){
			this.channelShop = channelShop;
		}
		
    @Column(name = "channel_store")
	public Boolean  getChannelStore(){
		return channelStore;
	} 
		
	public void setChannelStore(Boolean channelStore){
			this.channelStore = channelStore;
		}
		
    @Column(name = "first_cat_id")
	public Long  getFirstCatId(){
		return firstCatId;
	} 
		
	public void setFirstCatId(Long firstCatId){
			this.firstCatId = firstCatId;
		}
		
    @Column(name = "second_cat_id")
	public Long  getSecondCatId(){
		return secondCatId;
	} 
		
	public void setSecondCatId(Long secondCatId){
			this.secondCatId = secondCatId;
		}
		
    @Column(name = "third_cat_id")
	public Long  getThirdCatId(){
		return thirdCatId;
	} 
		
	public void setThirdCatId(Long thirdCatId){
			this.thirdCatId = thirdCatId;
		}
		
    @Column(name = "unit_id")
	public String  getUnitId(){
		return unitId;
	} 
		
	public void setUnitId(String unitId){
			this.unitId = unitId;
		}
		
    @Column(name = "unit")
	public String  getUnit(){
		return unit;
	} 
		
	public void setUnit(String unit){
			this.unit = unit;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "price")
	public java.math.BigDecimal  getPrice(){
		return price;
	} 
		
	public void setPrice(java.math.BigDecimal price){
			this.price = price;
		}
		
    @Column(name = "costprice")
	public java.math.BigDecimal  getCostprice(){
		return costprice;
	} 
		
	public void setCostprice(java.math.BigDecimal costprice){
			this.costprice = costprice;
		}
		
    @Column(name = "marketprice")
	public java.math.BigDecimal  getMarketprice(){
		return marketprice;
	} 
		
	public void setMarketprice(java.math.BigDecimal marketprice){
			this.marketprice = marketprice;
		}
		
    @Column(name = "description")
	public String  getDescription(){
		return description;
	} 
		
	public void setDescription(String description){
			this.description = description;
		}
		
    @Column(name = "brief")
	public String  getBrief(){
		return brief;
	} 
		
	public void setBrief(String brief){
			this.brief = brief;
		}
		
    @Column(name = "buys")
	public Long  getBuys(){
		return buys;
	} 
		
	public void setBuys(Long buys){
			this.buys = buys;
		}
		
    @Column(name = "comments")
	public Long  getComments(){
		return comments;
	} 
		
	public void setComments(Long comments){
			this.comments = comments;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "images")
	public String  getImages(){
		return images;
	} 
		
	public void setImages(String images){
			this.images = images;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
		}
		
    @Column(name = "stocks")
	public Long  getStocks(){
		return stocks;
	} 
		
	public void setStocks(Long stocks){
			this.stocks = stocks;
		}
		
    @Column(name = "actual_stocks")
	public Long  getActualStocks(){
		return actualStocks;
	} 
		
	public void setActualStocks(Long actualStocks){
			this.actualStocks = actualStocks;
		}
		
    @Column(name = "stocks_arm")
	public Long  getStocksArm(){
		return stocksArm;
	} 
		
	public void setStocksArm(Long stocksArm){
			this.stocksArm = stocksArm;
		}
		
    @Column(name = "prod_type")
	public String  getProdType(){
		return prodType;
	} 
		
	public void setProdType(String prodType){
			this.prodType = prodType;
		}
		
    @Column(name = "visual_prod_type")
	public String  getVisualProdType(){
		return visualProdType;
	} 
		
	public void setVisualProdType(String visualProdType){
			this.visualProdType = visualProdType;
		}
		
    @Column(name = "parameter")
	public String  getParameter(){
		return parameter;
	} 
		
	public void setParameter(String parameter){
			this.parameter = parameter;
		}
		
    @Column(name = "brand_id")
	public Long  getBrandId(){
		return brandId;
	} 
		
	public void setBrandId(Long brandId){
			this.brandId = brandId;
		}
		
    @Column(name = "review_scores")
	public Long  getReviewScores(){
		return reviewScores;
	} 
		
	public void setReviewScores(Long reviewScores){
			this.reviewScores = reviewScores;
		}
		
    @Column(name = "support_dist")
	public Boolean  getSupportDist(){
		return supportDist;
	} 
		
	public void setSupportDist(Boolean supportDist){
			this.supportDist = supportDist;
		}
		
    @Column(name = "commission_rate_setting_select")
	public Long  getCommissionRateSettingSelect(){
		return commissionRateSettingSelect;
	} 
		
	public void setCommissionRateSettingSelect(Long commissionRateSettingSelect){
			this.commissionRateSettingSelect = commissionRateSettingSelect;
		}
		
    @Column(name = "first_level_rate")
	public Double  getFirstLevelRate(){
		return firstLevelRate;
	} 
		
	public void setFirstLevelRate(Double firstLevelRate){
			this.firstLevelRate = firstLevelRate;
		}
		
    @Column(name = "second_level_rate")
	public Double  getSecondLevelRate(){
		return secondLevelRate;
	} 
		
	public void setSecondLevelRate(Double secondLevelRate){
			this.secondLevelRate = secondLevelRate;
		}
		
    @Column(name = "third_level_rate")
	public Double  getThirdLevelRate(){
		return thirdLevelRate;
	} 
		
	public void setThirdLevelRate(Double thirdLevelRate){
			this.thirdLevelRate = thirdLevelRate;
		}
		
    @Column(name = "store_id")
	public Long  getStoreId(){
		return storeId;
	} 
		
	public void setStoreId(Long storeId){
			this.storeId = storeId;
		}
		
    @Column(name = "store_category")
	public Long  getStoreCategory(){
		return storeCategory;
	} 
		
	public void setStoreCategory(Long storeCategory){
			this.storeCategory = storeCategory;
		}
		
    @Column(name = "guide_id")
	public Long  getGuideId(){
		return guideId;
	} 
		
	public void setGuideId(Long guideId){
			this.guideId = guideId;
		}
		
    @Column(name = "content_m")
	public String  getContentM(){
		return contentM;
	} 
		
	public void setContentM(String contentM){
			this.contentM = contentM;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
		
    @Column(name = "modify_date")
	public Date  getModifyDate(){
		return modifyDate;
	} 
		
	public void setModifyDate(Date modifyDate){
			this.modifyDate = modifyDate;
		}
		
    @Column(name = "wx_a_code")
	public String  getWxACode(){
		return wxACode;
	} 
		
	public void setWxACode(String wxACode){
			this.wxACode = wxACode;
		}
		
    @Column(name = "audit_opinion")
	public String  getAuditOpinion(){
		return auditOpinion;
	} 
		
	public void setAuditOpinion(String auditOpinion){
			this.auditOpinion = auditOpinion;
		}
		
    @Column(name = "goods_type")
	public String  getGoodsType(){
		return goodsType;
	} 
		
	public void setGoodsType(String goodsType){
			this.goodsType = goodsType;
		}
		
    @Column(name = "customs_message_id")
	public Long  getCustomsMessageId(){
		return customsMessageId;
	} 
		
	public void setCustomsMessageId(Long customsMessageId){
			this.customsMessageId = customsMessageId;
		}
		
    @Column(name = "freight_type")
	public Long  getFreightType(){
		return freightType;
	} 
		
	public void setFreightType(Long freightType){
			this.freightType = freightType;
		}
		
    @Column(name = "transport_id")
	public Long  getTransportId(){
		return transportId;
	} 
		
	public void setTransportId(Long transportId){
			this.transportId = transportId;
		}
		
    @Column(name = "settled_freight")
	public java.math.BigDecimal  getSettledFreight(){
		return settledFreight;
	} 
		
	public void setSettledFreight(java.math.BigDecimal settledFreight){
			this.settledFreight = settledFreight;
		}
		
    @Column(name = "custom_duty_type")
	public String  getCustomDutyType(){
		return customDutyType;
	} 
		
	public void setCustomDutyType(String customDutyType){
			this.customDutyType = customDutyType;
		}
		
    @Column(name = "cross_border_type")
	public String  getCrossBorderType(){
		return crossBorderType;
	} 
		
	public void setCrossBorderType(String crossBorderType){
			this.crossBorderType = crossBorderType;
		}
		
    @Column(name = "enable_warranty_card")
	public Boolean  getEnableWarrantyCard(){
		return enableWarrantyCard;
	} 
		
	public void setEnableWarrantyCard(Boolean enableWarrantyCard){
			this.enableWarrantyCard = enableWarrantyCard;
		}
		
    @Column(name = "warranty_card_effective_time")
	public Long  getWarrantyCardEffectiveTime(){
		return warrantyCardEffectiveTime;
	} 
		
	public void setWarrantyCardEffectiveTime(Long warrantyCardEffectiveTime){
			this.warrantyCardEffectiveTime = warrantyCardEffectiveTime;
		}
		
    @Column(name = "supplier_spu_id")
	public Long  getSupplierSpuId(){
		return supplierSpuId;
	} 
		
	public void setSupplierSpuId(Long supplierSpuId){
			this.supplierSpuId = supplierSpuId;
		}
		
    @Column(name = "prod_source")
	public String  getProdSource(){
		return prodSource;
	} 
		
	public void setProdSource(String prodSource){
			this.prodSource = prodSource;
		}
		
    @Column(name = "float_type")
	public String  getFloatType(){
		return floatType;
	} 
		
	public void setFloatType(String floatType){
			this.floatType = floatType;
		}
		
    @Column(name = "float_value")
	public java.math.BigDecimal  getFloatValue(){
		return floatValue;
	} 
		
	public void setFloatValue(java.math.BigDecimal floatValue){
			this.floatValue = floatValue;
		}
		
    @Column(name = "display_supplier_information")
	public Boolean  getDisplaySupplierInformation(){
		return displaySupplierInformation;
	} 
		
	public void setDisplaySupplierInformation(Boolean displaySupplierInformation){
			this.displaySupplierInformation = displaySupplierInformation;
		}
		
    @Column(name = "delivery_type")
	public String  getDeliveryType(){
		return deliveryType;
	} 
		
	public void setDeliveryType(String deliveryType){
			this.deliveryType = deliveryType;
		}
		
    @Column(name = "support_employee_reward")
	public Boolean  getSupportEmployeeReward(){
		return supportEmployeeReward;
	} 
		
	public void setSupportEmployeeReward(Boolean supportEmployeeReward){
			this.supportEmployeeReward = supportEmployeeReward;
		}
		
    @Column(name = "taxes_switch")
	public Boolean  getTaxesSwitch(){
		return taxesSwitch;
	} 
		
	public void setTaxesSwitch(Boolean taxesSwitch){
			this.taxesSwitch = taxesSwitch;
		}
		
    @Column(name = "invalid_status")
	public Long  getInvalidStatus(){
		return invalidStatus;
	} 
		
	public void setInvalidStatus(Long invalidStatus){
			this.invalidStatus = invalidStatus;
		}
		
    @Column(name = "content_images")
	public String  getContentImages(){
		return contentImages;
	} 
		
	public void setContentImages(String contentImages){
			this.contentImages = contentImages;
		}
		
    @Column(name = "force_online")
	public Boolean  getForceOnline(){
		return forceOnline;
	} 
		
	public void setForceOnline(Boolean forceOnline){
			this.forceOnline = forceOnline;
		}
		
    @Column(name = "support_team_award")
	public Boolean  getSupportTeamAward(){
		return supportTeamAward;
	} 
		
	public void setSupportTeamAward(Boolean supportTeamAward){
			this.supportTeamAward = supportTeamAward;
		}
		
    @Column(name = "support_performance_award")
	public Boolean  getSupportPerformanceAward(){
		return supportPerformanceAward;
	} 
		
	public void setSupportPerformanceAward(Boolean supportPerformanceAward){
			this.supportPerformanceAward = supportPerformanceAward;
		}
		
    @Column(name = "video")
	public String  getVideo(){
		return video;
	} 
		
	public void setVideo(String video){
			this.video = video;
		}
		
    @Column(name = "market_id")
	public Long  getMarketId(){
		return marketId;
	} 
		
	public void setMarketId(Long marketId){
			this.marketId = marketId;
		}
		
    @Column(name = "supplier_id")
	public Long  getSupplierId(){
		return supplierId;
	} 
		
	public void setSupplierId(Long supplierId){
			this.supplierId = supplierId;
		}
		
    @Column(name = "second_supplier_id")
	public Long  getSecondSupplierId(){
		return secondSupplierId;
	} 
		
	public void setSecondSupplierId(Long secondSupplierId){
			this.secondSupplierId = secondSupplierId;
		}
		
    @Column(name = "json_info")
	public String  getJsonInfo(){
		return jsonInfo;
	} 
		
	public void setJsonInfo(String jsonInfo){
			this.jsonInfo = jsonInfo;
		}
		
    @Column(name = "sales_market_product_status")
	public Long  getSalesMarketProductStatus(){
		return salesMarketProductStatus;
	} 
		
	public void setSalesMarketProductStatus(Long salesMarketProductStatus){
			this.salesMarketProductStatus = salesMarketProductStatus;
		}
	
	@Transient
	public Long getId() {
		return prodId;
	}
	
	public void setId(Long id) {
		prodId = id;
	}


} 
