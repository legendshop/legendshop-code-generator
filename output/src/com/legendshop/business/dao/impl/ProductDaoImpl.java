/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.Product;
import com.legendshop.business.dao.ProductDao;

/**
 * The Class ProductDaoImpl. 商品Dao实现类
 */

public class ProductDaoImpl extends GenericDaoImpl<Product, Long> implements ProductDao{

	/**
	 * 根据Id获取商品
	 */
	public Product getProduct(Long id){
		return getById(id);
	}

	/**
	 *  删除商品
	 *  @param product 实体类
	 *  @return 删除结果
	 */	
    public int deleteProduct(Product product){
    	return delete(product);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteProduct(Long id){
		return deleteById(id);
	}

	/**
	 * 保存商品
	 */
	public Long saveProduct(Product product){
		return save(product);
	}

	/**
	 *  更新商品
	 */		
	public int updateProduct(Product product){
		return update(product);
	}

	/**
	 * 分页查询商品列表
	 */
	public PageSupport<Product>queryProduct(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(Product.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

}
