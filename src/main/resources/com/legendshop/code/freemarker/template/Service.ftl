/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package ${servicePackagePath};

import com.legendshop.dao.support.PageSupport;
import ${modelPackagePath}.${entityClass};

/**
 * The Class ${serviceClass}.
 * ${table.tableComment}服务接口
 */
public interface ${serviceClass}  {

   	/**
	 *  根据Id获取${table.tableComment}
	 */
    public ${entityClass} get${entityClass}(${IdType} id);

    /**
	 *  根据Id删除${table.tableComment}
	 */
    public int delete${entityClass}(${IdType} id);
    
    /**
	 *  根据对象删除${table.tableComment}
	 */
    public int delete${entityClass}(${entityClass} ${entityClassInstance});
    
   /**
	 *  保存${table.tableComment}
	 */	    
    public ${IdType} save${entityClass}(${entityClass} ${entityClassInstance});

   /**
	 *  更新${table.tableComment}
	 */	
    public void update${entityClass}(${entityClass} ${entityClassInstance});
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<${entityClass}> query${entityClass}(String curPageNO,Integer pageSize);
    
}
