/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package ${controllerPackagePath};

import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ${servicePackagePath}.${serviceClass};
import ${modelPackagePath}.${entityClass};

/**
 * The Class ${entityClass}Controller
 * ${table.tableComment}控制器
 */
@Controller
@RequestMapping("${actionPrex}/${entityClassInstance}")
public class ${entityClass}Controller extends BaseController{

	/** 日志. */
	private final Logger log = LoggerFactory.getLogger(${entityClass}.class);
	
    @Autowired
    private ${entityClass}Service ${entityClassInstance}Service;

	/**
	 * ${table.tableComment}列表查询
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		//TODO 按页面增加查询条件,并传递到后台的service
        PageSupport ps = ${entityClassInstance}Service.query${entityClass}(curPageNO, 10);//TODO 10为每页的记录数, 可以页面传递过来或者从PropertiesUtil中获取
        PageSupportHelper.savePage(request, ps);
        
        return "/${entityClassInstance}/${entityClassInstance}List";
        //TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
       // return PathResolver.getPath(request, response, BackPage.${entityClassInstance?upper_case}_LIST_PAGE);
    }

	/**
	 * 保存${table.tableComment}
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, ${entityClass} ${entityClassInstance}) {
    
      if(${entityClassInstance}.getId() != null){//update
		${entityClass} entity = ${entityClassInstance}Service.get${entityClass}(${entityClassInstance}.getId());
		if (entity != null) {
				//根据页面逻辑更新字段
				// TODO ......
				${entityClassInstance}Service.update${entityClass}(entity);
			}
		}else{//save
			//TODO 根据页面逻辑保存字段
			${entityClassInstance}Service.save${entityClass}(${entityClassInstance});
		}
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
    
        return "forward:${actionPrex}/${entityClassInstance}/query";
        //TODO, 返回页面,需要手动在FowardPage类中增加配置, 并注释上一行
		//return PathResolver.getPath(request, response, FowardPage.${entityClassInstance?upper_case}_LIST_QUERY);
    }

	/**
	 * 删除${table.tableComment}
	 */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable ${IdType} id) {
    	
    	${entityClass} ${entityClassInstance} = ${entityClassInstance}Service.get${entityClass}(id);
		if(${entityClassInstance} != null){
			log.info("{} delete ${entityClassInstance} by Id {}", ${entityClassInstance}.getId());
			${entityClassInstance}Service.delete${entityClass}(${entityClassInstance});
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
        return "redirect:${actionPrex}/${entityClassInstance}/query";
        //TODO, 返回页面,需要手动在RedirectPage类中增加配置, 并注释上一行
        //return PathResolver.getPath(request, response, RedirectPage.${entityClassInstance?upper_case}_LIST_QUERY);
    }

	/**
	 * 根据Id加载${table.tableComment}
	 */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable ${IdType} id) {
    	//TODO 如果需要,则要增加权限配置
        ${entityClass} ${entityClassInstance} = ${entityClassInstance}Service.get${entityClass}(id);
        request.setAttribute("${entityClassInstance}", ${entityClassInstance});
        return "/${entityClassInstance}/${entityClassInstance}";
        //TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
        //return PathResolver.getPath(request, response, BackPage.${entityClassInstance?upper_case}_EDIT_PAGE);
    }
    
   /**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/${entityClassInstance}/${entityClassInstance}";
		//TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
		//return PathResolver.getPath(request, response, BackPage.${entityClassInstance?upper_case}_EDIT_PAGE);
	}


}
