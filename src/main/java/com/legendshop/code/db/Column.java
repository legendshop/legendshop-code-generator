package com.legendshop.code.db;


public class Column {
	private String columnName;
	private String dataType;
	private long numericPrecision;
	
	private String columnComment;

	private String javaSimpleType;
	private String javaType;
	private String javaName;
	private String commit;

	private Object defaultValue;
	private boolean isNullable;
	private boolean isPrimaryKeyColumn;

	@Override
	public String toString() {
		return "Column [columnName=" + columnName + ", dataType=" + dataType + ", numericPrecision=" + numericPrecision
				+ ", columnComment=" + columnComment + ", javaSimpleType=" + javaSimpleType + ", javaType=" + javaType
				+ ", javaName=" + javaName + ", commit=" + commit + ", defaultValue=" + defaultValue + ", isNullable=" + isNullable
				+ ", isPrimaryKeyColumn=" + isPrimaryKeyColumn + "]";
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getJavaSimpleType() {
		return javaSimpleType;
	}

	public void setJavaSimpleType(String javaSimpleType) {
		this.javaSimpleType = javaSimpleType;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getJavaName() {
		return javaName;
	}

	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}

	public String getCommit() {
		return commit;
	}

	public void setCommit(String commit) {
		this.commit = commit;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public long getNumericPrecision() {
		return numericPrecision;
	}

	public void setNumericPrecision(long numericPrecision) {
		this.numericPrecision = numericPrecision;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

	public boolean isNullable() {
		return isNullable;
	}

	public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	public boolean isPrimaryKeyColumn() {
		return isPrimaryKeyColumn;
	}

	public void setPrimaryKeyColumn(boolean isPrimaryKeyColumn) {
		this.isPrimaryKeyColumn = isPrimaryKeyColumn;
	}

}
