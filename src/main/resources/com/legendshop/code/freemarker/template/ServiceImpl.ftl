/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package ${servicePackagePath}.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;
import ${daoPackagePath}.${daoClass};
import ${modelPackagePath}.${entityClass};
import ${servicePackagePath}.${entityClass}Service;

/**
 * The Class ${serviceClass}Impl.
 *  ${table.tableComment}服务实现类
 */
public class ${serviceClass}Impl implements ${entityClass}Service{

    /**
     *
     * 引用的${table.tableComment}Dao接口
     */
    private ${daoClass} ${daoClassInstance};
   
   	/**
	 *  根据Id获取${table.tableComment}
	 */
    public ${entityClass} get${entityClass}(${IdType} id) {
        return ${daoClassInstance}.get${entityClass}(id);
    }
    
    /**
	 *  根据Id删除${table.tableComment}
	 */
    public int delete${entityClass}(${IdType} id){
    	return ${daoClassInstance}.delete${entityClass}(id);
    }

   /**
	 *  删除${table.tableComment}
	 */ 
    public int delete${entityClass}(${entityClass} ${entityClassInstance}) {
       return  ${daoClassInstance}.delete${entityClass}(${entityClassInstance});
    }

   /**
	 *  保存${table.tableComment}
	 */	    
    public ${IdType} save${entityClass}(${entityClass} ${entityClassInstance}) {
        if (!AppUtils.isBlank(${entityClassInstance}.${getId}())) {
            update${entityClass}(${entityClassInstance});
            return ${entityClassInstance}.${getId}();
        }
        return ${daoClassInstance}.save${entityClass}(${entityClassInstance});
    }

   /**
	 *  更新${table.tableComment}
	 */	
    public void update${entityClass}(${entityClass} ${entityClassInstance}) {
        ${daoClassInstance}.update${entityClass}(${entityClassInstance});
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<${entityClass}> query${entityClass}(String curPageNO, Integer pageSize){
     	return ${daoClassInstance}.query${entityClass}(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void set${daoClass}(${daoClass} ${daoClassInstance}) {
        this.${daoClassInstance} = ${daoClassInstance};
    }
    
}
