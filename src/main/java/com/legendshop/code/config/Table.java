package com.legendshop.code.config;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.code.db.Column;

public class Table {
	private List<Column> columnList = new ArrayList<Column>();
	private String tableName;
	private String tableJavaName;
	private String tableComment;

	public List<Column> getColumnList() {
		return columnList;
	}

	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableJavaName() {
		return tableJavaName;
	}

	public void setTableJavaName(String tableJavaName) {
		this.tableJavaName = tableJavaName;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

}