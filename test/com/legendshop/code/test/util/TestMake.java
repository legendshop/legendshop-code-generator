/**
 * 
 */
package com.legendshop.code.test.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class TestMake {

    String sDBDriver;

    String sConnStr;

    Connection conn;

    ResultSet rs;

    String User;

    String Pass;

    /**
     * 
     */
    public TestMake() {
        sDBDriver = "oracle.jdbc.driver.OracleDriver";
        sConnStr = "jdbc:oracle:thin:@localhost:1521:hewqdb";
        conn = null;
        rs = null;
        User = "portal";
        Pass = "portal";
        try {
            Class.forName(sDBDriver);
        } catch (ClassNotFoundException classnotfoundexception) {
            System.err.println("connectDB(): " + classnotfoundexception.getMessage());
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        TestMake testMake = new TestMake();
        try {
            Connection con = testMake.getConnection();
            DatabaseMetaData dmd = con.getMetaData();
            System.out.println(dmd.getDatabaseMajorVersion());
            System.out.println(dmd.getCatalogs());
            System.out.println(dmd.getCatalogSeparator());
            System.out.println(dmd.getConnection());
            System.out.println(dmd.getDriverName());
            System.out.println(dmd.getTableTypes());
            System.out.println(testMake.getConnection());
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM user_logon_history");
            for (int i = 0; rs.next(); i++) {
                ;
            }
            //	while(rs.next()){
            //int count=rs.getInt(1);
            ResultSetMetaData rmd = rs.getMetaData();
            System.out.println("rmd.getTableName = " + rmd.getTableName(1));
            System.out.println("rmd.getColumnCount = " + rmd.getColumnCount());
            for (int i = 1; i <= rmd.getColumnCount(); i++) {
                System.out.println("rmd.getColumnName = " + i + " : " + testMake.toBoString(rmd.getColumnName(i)));
            }
            //System.out.println("count = "+count);
            //}

            String test[] = { "adminUser", "adffAd11" };
            System.out.println(testMake.toDataBaseString(test));
            String[] r = testMake.toDataBaseString(test);
            for (int i = 0; i < test.length; i++) {
                System.out.println(r[i]);
            }

            String[] r2 = testMake.toBoString(r);
            for (int i = 0; i < test.length; i++) {
                System.out.println(r2[i]);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public String setFirstCharUpcase(String s) {
        if ((s == null) || (s.length() < 1)) {
            return s;
        }
        char c[] = s.toCharArray();
        if ((c.length > 0) && (c[0] >= 'a') && (c[0] <= 'z')) {
            c[0] = (char) ((short) c[0] - 32);
        }
        return String.valueOf(c);
    }

    public String[] toDataBaseString(String[] strs) {
        if ((strs == null) || (strs.length <= 0)) {
            return null;
        }
        String[] newStr = new String[strs.length];
        for (int i = 0; i < strs.length; i++) {
            newStr[i] = toDataBaseString(strs[i]);
        }
        return newStr;
    }

    public String toDataBaseString(String str) {
        if ((str == null) || (str.trim().length() <= 0)) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        char[] cs = str.toCharArray();
        for (char element : cs) {
            if (Character.isUpperCase(element)) {//��д
                sb.append("_").append(element);
            } else if (Character.isLowerCase(element)) {//Сд
                sb.append(Character.toUpperCase(element));
            } else {//���������
                sb.append(element);
            }

        }

        return sb.toString();
    }

    public String[] toBoString(String[] strs) {
        if ((strs == null) || (strs.length <= 0)) {
            return null;
        }
        String[] newStr = new String[strs.length];
        for (int i = 0; i < strs.length; i++) {
            newStr[i] = toBoString(strs[i]);
        }
        return newStr;
    }

    public String toBoString(String str) {
        if ((str == null) || (str.trim().length() <= 0)) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        char[] cs = str.toLowerCase().toCharArray();
        for (int i = 0; i < cs.length; i++) {
            if ((cs[i] == '_') && (i == cs.length - 1)) {
                sb.append(cs[i]);
            } else if ((cs[i] == '_') && (i < cs.length - 1)) {
                sb.append(Character.toUpperCase(cs[i + 1]));
                i++;
            } else {
                sb.append(cs[i]);
            }

        }

        return sb.toString();
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(sConnStr, User, Pass);
    }
}
