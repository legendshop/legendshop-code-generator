/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.business.service.ProductService;
import com.legendshop.model.entity.Product;

/**
 * The Class ProductController
 * 商品控制器
 */
@Controller
@RequestMapping("/admin/product")
public class ProductController extends BaseController{

	/** 日志. */
	private final Logger log = LoggerFactory.getLogger(Product.class);
	
    @Autowired
    private ProductService productService;

	/**
	 * 商品列表查询
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		//TODO 按页面增加查询条件,并传递到后台的service
        PageSupport ps = productService.queryProduct(curPageNO, 10);//TODO 10为每页的记录数, 可以页面传递过来或者从PropertiesUtil中获取
        PageSupportHelper.savePage(request, ps);
        
        return "/product/productList";
        //TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
       // return PathResolver.getPath(request, response, BackPage.PRODUCT_LIST_PAGE);
    }

	/**
	 * 保存商品
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Product product) {
    
      if(product.getId() != null){//update
		Product entity = productService.getProduct(product.getId());
		if (entity != null) {
				//根据页面逻辑更新字段
				// TODO ......
				productService.updateProduct(entity);
			}
		}else{//save
			//TODO 根据页面逻辑保存字段
			productService.saveProduct(product);
		}
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
    
        return "forward:/admin/product/query";
        //TODO, 返回页面,需要手动在FowardPage类中增加配置, 并注释上一行
		//return PathResolver.getPath(request, response, FowardPage.PRODUCT_LIST_QUERY);
    }

	/**
	 * 删除商品
	 */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
    	
    	Product product = productService.getProduct(id);
		if(product != null){
			log.info("{} delete product by Id {}", product.getId());
			productService.deleteProduct(product);
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
        return "redirect:/admin/product/query";
        //TODO, 返回页面,需要手动在RedirectPage类中增加配置, 并注释上一行
        //return PathResolver.getPath(request, response, RedirectPage.PRODUCT_LIST_QUERY);
    }

	/**
	 * 根据Id加载商品
	 */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
    	//TODO 如果需要,则要增加权限配置
        Product product = productService.getProduct(id);
        request.setAttribute("product", product);
        return "/product/product";
        //TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
        //return PathResolver.getPath(request, response, BackPage.PRODUCT_EDIT_PAGE);
    }
    
   /**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/product/product";
		//TODO, 返回页面,需要手动在BackPage类中增加配置, 并注释上一行
		//return PathResolver.getPath(request, response, BackPage.PRODUCT_EDIT_PAGE);
	}


}
