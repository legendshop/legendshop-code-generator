<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
    <meta charset="utf-8">
    <title>热门商品 - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="商品">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x: auto;">
        <table class="${tableclass} title-border" style="width: 100%">
            <tr>
                <th class="title-border">首页管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"><a href="${contextPath}/admin/product/query">商品</a></span></th>
            </tr>
        </table>
        <form:form action="${contextPath}/admin/product/query" id="form1" method="post">
            <div class="criteria-div">
						<span class="item">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							示例查询条件：<input class="${inputclass}" type="text" name="title" maxlength="50" value="${bean.title}" size="30" />
							<input class="${btnclass}" type="button" onclick="search()" value="搜索" />
							<input class="${btnclass}" type="button" value="创建商品" onclick='window.location="<ls:url address='/admin/product/load'/>"' />
						</span>
            </div>
        </form:form>
        <div align="center" class="order-content-list">
            <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
            <display:table name="list" requestURI="${contextPath}/admin/product/query" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
         		  <display:column title="ProdId" property="prodId"></display:column>
         		  <display:column title="Version" property="version"></display:column>
         		  <display:column title="ChannelShop" property="channelShop"></display:column>
         		  <display:column title="ChannelStore" property="channelStore"></display:column>
         		  <display:column title="FirstCatId" property="firstCatId"></display:column>
         		  <display:column title="SecondCatId" property="secondCatId"></display:column>
         		  <display:column title="ThirdCatId" property="thirdCatId"></display:column>
         		  <display:column title="UnitId" property="unitId"></display:column>
         		  <display:column title="Unit" property="unit"></display:column>
         		  <display:column title="ShopId" property="shopId"></display:column>
         		  <display:column title="Name" property="name"></display:column>
         		  <display:column title="Price" property="price"></display:column>
         		  <display:column title="Costprice" property="costprice"></display:column>
         		  <display:column title="Marketprice" property="marketprice"></display:column>
         		  <display:column title="Description" property="description"></display:column>
         		  <display:column title="Brief" property="brief"></display:column>
         		  <display:column title="Buys" property="buys"></display:column>
         		  <display:column title="Comments" property="comments"></display:column>
         		  <display:column title="Pic" property="pic"></display:column>
         		  <display:column title="Images" property="images"></display:column>
         		  <display:column title="Status" property="status"></display:column>
         		  <display:column title="Stocks" property="stocks"></display:column>
         		  <display:column title="ActualStocks" property="actualStocks"></display:column>
         		  <display:column title="StocksArm" property="stocksArm"></display:column>
         		  <display:column title="ProdType" property="prodType"></display:column>
         		  <display:column title="VisualProdType" property="visualProdType"></display:column>
         		  <display:column title="Parameter" property="parameter"></display:column>
         		  <display:column title="BrandId" property="brandId"></display:column>
         		  <display:column title="ReviewScores" property="reviewScores"></display:column>
         		  <display:column title="SupportDist" property="supportDist"></display:column>
         		  <display:column title="CommissionRateSettingSelect" property="commissionRateSettingSelect"></display:column>
         		  <display:column title="FirstLevelRate" property="firstLevelRate"></display:column>
         		  <display:column title="SecondLevelRate" property="secondLevelRate"></display:column>
         		  <display:column title="ThirdLevelRate" property="thirdLevelRate"></display:column>
         		  <display:column title="StoreId" property="storeId"></display:column>
         		  <display:column title="StoreCategory" property="storeCategory"></display:column>
         		  <display:column title="GuideId" property="guideId"></display:column>
         		  <display:column title="ContentM" property="contentM"></display:column>
         		  <display:column title="Content" property="content"></display:column>
         		  <display:column title="RecDate" property="recDate"></display:column>
         		  <display:column title="ModifyDate" property="modifyDate"></display:column>
         		  <display:column title="WxACode" property="wxACode"></display:column>
         		  <display:column title="AuditOpinion" property="auditOpinion"></display:column>
         		  <display:column title="GoodsType" property="goodsType"></display:column>
         		  <display:column title="CustomsMessageId" property="customsMessageId"></display:column>
         		  <display:column title="FreightType" property="freightType"></display:column>
         		  <display:column title="TransportId" property="transportId"></display:column>
         		  <display:column title="SettledFreight" property="settledFreight"></display:column>
         		  <display:column title="CustomDutyType" property="customDutyType"></display:column>
         		  <display:column title="CrossBorderType" property="crossBorderType"></display:column>
         		  <display:column title="EnableWarrantyCard" property="enableWarrantyCard"></display:column>
         		  <display:column title="WarrantyCardEffectiveTime" property="warrantyCardEffectiveTime"></display:column>
         		  <display:column title="SupplierSpuId" property="supplierSpuId"></display:column>
         		  <display:column title="ProdSource" property="prodSource"></display:column>
         		  <display:column title="FloatType" property="floatType"></display:column>
         		  <display:column title="FloatValue" property="floatValue"></display:column>
         		  <display:column title="DisplaySupplierInformation" property="displaySupplierInformation"></display:column>
         		  <display:column title="DeliveryType" property="deliveryType"></display:column>
         		  <display:column title="SupportEmployeeReward" property="supportEmployeeReward"></display:column>
         		  <display:column title="TaxesSwitch" property="taxesSwitch"></display:column>
         		  <display:column title="InvalidStatus" property="invalidStatus"></display:column>
         		  <display:column title="ContentImages" property="contentImages"></display:column>
         		  <display:column title="ForceOnline" property="forceOnline"></display:column>
         		  <display:column title="SupportTeamAward" property="supportTeamAward"></display:column>
         		  <display:column title="SupportPerformanceAward" property="supportPerformanceAward"></display:column>
         		  <display:column title="Video" property="video"></display:column>
         		  <display:column title="MarketId" property="marketId"></display:column>
         		  <display:column title="SupplierId" property="supplierId"></display:column>
         		  <display:column title="SecondSupplierId" property="secondSupplierId"></display:column>
         		  <display:column title="JsonInfo" property="jsonInfo"></display:column>
         		  <display:column title="SalesMarketProductStatus" property="salesMarketProductStatus"></display:column>
                <display:column title="操作" media="html" style="width:300px">
                    <div class="table-btn-group">
                        <button class="tab-btn" onclick="window.location='${contextPath}/admin/product/load/${item.id}'">修改</button>
                        <span class="btn-line">|</span>
                        <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
                    </div>
                </display:column>
            </display:table>
            <div class="clearfix">
                <div class="fr">
                    <div class="page">
                        <div class="p-wrap">
                            <ls:page pageSize="${pageSize}"  total="${total}" curPageNO="${curPageNO}"  type="simple"/>
                        </div>
                    </div>
                </div>
            </div>
            <table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
                <tr>
                    <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 
                    1、 xxxxxxxx<br>
                    2、 xxxxxxxx。
                    </td>
                <tr>
            </table>
        </div>
    </div>
</div>
</body>

<script language="JavaScript" type="text/javascript">
    $(document).ready(function() {

    });

    function deleteById(id) {
        layer.confirm('确定给删除？', {icon:3, title:'提示'}, function(){
            window.location = "${contextPath}/admin/product/delete/" + id;
        });
    }

    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>