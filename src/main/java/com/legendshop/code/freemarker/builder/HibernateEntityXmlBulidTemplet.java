package com.legendshop.code.freemarker.builder;

import java.util.Map;


import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.Templet;

@Templet
public class HibernateEntityXmlBulidTemplet extends AbstractBulidTemplet {

	public HibernateEntityXmlBulidTemplet(){
		
	}
	
	@Override
	public boolean buildSourceFile(String tableName) {
		//System.out.println("Start Building HibernateEntityXml File.");

		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.MAIN_PATH + PackageConfigUtil.getModelPackagePath();
		String templateFileName = "HibernateEntityXml.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);
		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("columns", table.getColumnList());
		paramMap.put("table", table);

		String entityClass = (String) paramMap.get("entityClass");

		String fileName = entityClass + ".hbm.xml";

		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,false);

		//System.out.println("End Build HibernateEntityXml File.\n");
		return result;
	}

}
