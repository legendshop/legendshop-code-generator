/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Product;

/**
 * The Class ProductDao. 商品Dao接口
 */
public interface ProductDao extends Dao<Product,Long>{

	/**
	 * 根据Id获取商品
	 */
	public abstract Product getProduct(Long id);

	/**
	 *  根据Id删除商品
	 */
    public abstract int deleteProduct(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteProduct(Product product);

	/**
	 * 保存商品
	 */
	public abstract Long saveProduct(Product product);

	/**
	 *  更新商品
	 */		
	public abstract int updateProduct(Product product);

	/**
	 * 分页查询商品列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<Product> queryProduct(String curPageNO, Integer pageSize);

}
