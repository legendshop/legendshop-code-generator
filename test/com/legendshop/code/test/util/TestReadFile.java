package com.legendshop.code.test.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.oro.text.regex.MalformedPatternException;

import com.legendshop.code.util.StringConverter;

public class TestReadFile {

    public static void main(String[] args) throws MalformedPatternException {
        HashMap map = new HashMap();
        map.put("#daoName#", "DAO");
        map.put("#hha#", "cat");

        String pattern = "\\#[a-zA-Z]+\\#";
        String text = "public class #daoName# extends BaseDao {";
        String result = StringConverter.convert(text, pattern, map);
        System.out.println("result = " + result);

        TestReadFile test = new TestReadFile();
        test.readMyFile("template/Dao.template");
    }

    void readMyFile(String fileName) {
        String record = null;
        int recCount = 0;
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            record = new String();
            while ((record = br.readLine()) != null) {
                recCount++;
                System.out.println(recCount + ": " + record);
            }
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println("Uh oh, got an IOException error!");
            e.printStackTrace();
        }
    }
}
