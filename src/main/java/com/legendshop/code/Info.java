package com.legendshop.code;

public class Info {
    private String table;
    private String id;
    private String sequenceName;

    public Info() {

    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    @Override
    public String toString() {
        return "table = " + table + ", id = " + id + ", sequenceName = " + sequenceName;
    }
}
