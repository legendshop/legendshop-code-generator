<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
    <meta charset="utf-8">
    <title>创建商品 - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="商品">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/product/save" method="post" id="form1">
            <input id="id" name="id" value="${bean.id}" type="hidden">
            <div align="center">
                <table class="${tableclass}" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="no-bg title-border">首页管理&nbsp;＞&nbsp;
	                        <span style="color:#0e90d2; font-size:14px;"><a href="${contextPath}/admin/product/query">商品</a></span>&nbsp;＞&nbsp;
	                        <span style="color:#0e90d2;">创建商品</span>
                        </th>
                    </tr>
                    </thead>
                </table>
                <table  align="center" class="${tableclass} no-border" id="col1" style="width: 100%">
                
            		<tr>
            		        <td width="15%">
            		          	<div align="right">产品版本: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="version" id="version" value="${product.version}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">线上渠道: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="channelShop" id="channelShop" value="${product.channelShop}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">线下渠道: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="channelStore" id="channelStore" value="${product.channelStore}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品一级分类: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="firstCatId" id="firstCatId" value="${product.firstCatId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品二级分类: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="secondCatId" id="secondCatId" value="${product.secondCatId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品三级分类: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="thirdCatId" id="thirdCatId" value="${product.thirdCatId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">计量单位Id: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="unitId" id="unitId" value="${product.unitId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">计量单位: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="unit" id="unit" value="${product.unit}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商家ID: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="shopId" id="shopId" value="${product.shopId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品名字: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="name" id="name" value="${product.name}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">销售价格: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="price" id="price" value="${product.price}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">成本价格: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="costprice" id="costprice" value="${product.costprice}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">市场价格(划线价): <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="marketprice" id="marketprice" value="${product.marketprice}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">活动标签: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="description" id="description" value="${product.description}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品卖点: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="brief" id="brief" value="${product.brief}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">已经销售数量: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="buys" id="buys" value="${product.buys}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">评论数: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="comments" id="comments" value="${product.comments}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品图片: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="pic" id="pic" value="${product.pic}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">图片列表: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="images" id="images" value="${product.images}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">产品状态， 1: 上架，0: 下架， 2：被平台违规下架， -1：商品被逻辑删除， 回收站的商品； 参见ProductStatusEnum枚举: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="status" id="status" value="${product.status}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">销售库存: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="stocks" id="stocks" value="${product.stocks}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">实际库存: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="actualStocks" id="actualStocks" value="${product.actualStocks}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">库存警告: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="stocksArm" id="stocksArm" value="${product.stocksArm}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品类型，P.普通商品，G:团购商品,参见ProductTypeEnum: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="prodType" id="prodType" value="${product.prodType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">虚拟商品类型，参见VisualProductTypeEnum: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="visualProdType" id="visualProdType" value="${product.visualProdType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品动态参数: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="parameter" id="parameter" value="${product.parameter}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">品牌: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="brandId" id="brandId" value="${product.brandId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">评论得分: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="reviewScores" id="reviewScores" value="${product.reviewScores}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否支持分销: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supportDist" id="supportDist" value="${product.supportDist}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">佣金设置选择  0默认  1自定义: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="commissionRateSettingSelect" id="commissionRateSettingSelect" value="${product.commissionRateSettingSelect}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">会员直接上级分佣比例: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="firstLevelRate" id="firstLevelRate" value="${product.firstLevelRate}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">会员上二级分佣比例: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="secondLevelRate" id="secondLevelRate" value="${product.secondLevelRate}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">会员上三级分佣比例: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="thirdLevelRate" id="thirdLevelRate" value="${product.thirdLevelRate}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">门店Id: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="storeId" id="storeId" value="${product.storeId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">门店分类Id: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="storeCategory" id="storeCategory" value="${product.storeCategory}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">导购Id，导购上传的商品: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="guideId" id="guideId" value="${product.guideId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">手机端商品详情: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="contentM" id="contentM" value="${product.contentM}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">PC端商品详情: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="content" id="content" value="${product.content}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">录入时间: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="recDate" id="recDate" value="${product.recDate}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">修改时间: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="modifyDate" id="modifyDate" value="${product.modifyDate}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品的微信小程序码: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="wxACode" id="wxACode" value="${product.wxACode}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">审核意见: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="auditOpinion" id="auditOpinion" value="${product.auditOpinion}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品类型  GoodsTypeEnum  normal：普通  overseas：海淘: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="goodsType" id="goodsType" value="${product.goodsType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">关联的海关信息ID: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="customsMessageId" id="customsMessageId" value="${product.customsMessageId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">运费设置类型   FreightTypeEnum  0:商家承担运费 1：使用商家运费规则 2：固定运费: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="freightType" id="freightType" value="${product.freightType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">运费规则ID  当freightType为1才有值: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="transportId" id="transportId" value="${product.transportId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">固定的运费值 当freightType为2才有值: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="settledFreight" id="settledFreight" value="${product.settledFreight}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">进口税类型,CustomDutyTypeEnum   DUTY_INCLUDE：商品含税，EXTRA_INCLUDE：独立计算税额: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="customDutyType" id="customDutyType" value="${product.customDutyType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">跨境模式,CrossBorderTypeEnum  BONDED_WAREHOUSE：保税仓备货, DIRECT_MAIL: 海外直邮: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="crossBorderType" id="crossBorderType" value="${product.crossBorderType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否开启质保卡: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="enableWarrantyCard" id="enableWarrantyCard" value="${product.enableWarrantyCard}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">质保卡有效时长, 单位是天: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="warrantyCardEffectiveTime" id="warrantyCardEffectiveTime" value="${product.warrantyCardEffectiveTime}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">供应商商品id: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supplierSpuId" id="supplierSpuId" value="${product.supplierSpuId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">供应商商品来源：SELF 自有, INNER 内部供应商, JD 京东, SUNING 苏宁  CARDCOUPON (卡券): <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="prodSource" id="prodSource" value="${product.prodSource}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">供应商商品价格上浮类型: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="floatType" id="floatType" value="${product.floatType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">供应商商品价格上浮值: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="floatValue" id="floatValue" value="${product.floatValue}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否显示供应商信息: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="displaySupplierInformation" id="displaySupplierInformation" value="${product.displaySupplierInformation}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">配送方式 ProductDeliveryTypeEnum: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="deliveryType" id="deliveryType" value="${product.deliveryType}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否参加 员工奖: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supportEmployeeReward" id="supportEmployeeReward" value="${product.supportEmployeeReward}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">税费开关: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="taxesSwitch" id="taxesSwitch" value="${product.taxesSwitch}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">失效状态, 0: 正常商品， 10：已失效: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="invalidStatus" id="invalidStatus" value="${product.invalidStatus}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">移动端详情图片集合: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="contentImages" id="contentImages" value="${product.contentImages}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否强制上架: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="forceOnline" id="forceOnline" value="${product.forceOnline}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否参加分销团队奖: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supportTeamAward" id="supportTeamAward" value="${product.supportTeamAward}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">是否参加业绩奖: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supportPerformanceAward" id="supportPerformanceAward" value="${product.supportPerformanceAward}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品视频url: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="video" id="video" value="${product.video}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">分销市场ID: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="marketId" id="marketId" value="${product.marketId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">供应商ID: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="supplierId" id="supplierId" value="${product.supplierId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">二级供应商ID: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="secondSupplierId" id="secondSupplierId" value="${product.secondSupplierId}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品拓展信息: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="jsonInfo" id="jsonInfo" value="${product.jsonInfo}" />
            		        </td>
	            		</tr>
            		<tr>
            		        <td width="15%">
            		          	<div align="right">商品在分销市场的状态，1：正常，0：异常: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="salesMarketProductStatus" id="salesMarketProductStatus" value="${product.salesMarketProductStatus}" />
            		        </td>
	            		</tr>
                 <tr>
                        <td colspan="2">
                            <div align="center">
                                <input class="${btnclass}" type="submit" value="保存" />
                                <input class="${btnclass}" type="button" value="返回" onclick="window.location='${contextPath}/admin/product/query'" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form:form>
    </div>
</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script language="javascript">
    jQuery.validator.setDefaults({});
    jQuery(document).ready(function() {
        jQuery("#form1").validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 50
                }
            },
            messages: {
                title: {
                    required: "请输入标题",
                    maxlength: "最大长度为50"
                }
            }
        });
    });
</script>
</html>
