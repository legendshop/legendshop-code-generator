/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.model.entity.Product;
import com.legendshop.business.service.ProductService;

/**
 * The Class ProductServiceImpl.
 *  商品服务实现类
 */
public class ProductServiceImpl implements ProductService{

    /**
     *
     * 引用的商品Dao接口
     */
    private ProductDao productDao;
   
   	/**
	 *  根据Id获取商品
	 */
    public Product getProduct(Long id) {
        return productDao.getProduct(id);
    }
    
    /**
	 *  根据Id删除商品
	 */
    public int deleteProduct(Long id){
    	return productDao.deleteProduct(id);
    }

   /**
	 *  删除商品
	 */ 
    public int deleteProduct(Product product) {
       return  productDao.deleteProduct(product);
    }

   /**
	 *  保存商品
	 */	    
    public Long saveProduct(Product product) {
        if (!AppUtils.isBlank(product.getProdId())) {
            updateProduct(product);
            return product.getProdId();
        }
        return productDao.saveProduct(product);
    }

   /**
	 *  更新商品
	 */	
    public void updateProduct(Product product) {
        productDao.updateProduct(product);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<Product> queryProduct(String curPageNO, Integer pageSize){
     	return productDao.queryProduct(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }
    
}
