package com.legendshop.code.freemarker;


public interface BulidTemplet {

	public final static String MAIN_PATH = "/src";
	public final static String MAIN_CONF = "/src/main/conf";
	public final static String ROOT = "/src";
	public final static String MAIN_PAGE = "/src/pages";
	public final static String TEST_PATH = "/src/test/java";
	public final static String TEST_CONF = "/src/test/conf";

	public abstract boolean buildSourceFile(String tableName);

}