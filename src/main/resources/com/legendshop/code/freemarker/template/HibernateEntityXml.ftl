<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd">
<!-- 
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
  -->
<hibernate-mapping>
	<class name="${modelPackagePath}.${entityClass}" table="${table.tableName}">
<#list columns as column>
	<#if column.isPrimaryKeyColumn()>
		<id name="${column.javaName}" column="${column.columnName}" type="${column.javaType}">
			<generator class="native" />
		</id>
    <#else>
    	
    	<#if column.javaSimpleType == 'Boolean'>
    		<property name="${column.javaName}" column="${column.columnName}" type="yes_no" />
    	<#else>
    		<property name="${column.javaName}" column="${column.columnName}" type="${column.javaType}" />
    	</#if>
    </#if>
</#list>
	</class>
</hibernate-mapping>
