package com.legendshop.code.freemarker.model;

import java.io.Serializable;

public class SQLStatement implements Serializable{

	private static final long serialVersionUID = -394501528028149075L;

	private String insert;
	
	private String update;
	
	private String delete;
	
	private String getById;
	
	private String getAdJavaNameById;

	public String getInsert() {
		return insert;
	}

	public void setInsert(String insert) {
		this.insert = insert;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public String getGetById() {
		return getById;
	}

	public void setGetById(String getById) {
		this.getById = getById;
	}

	public String getGetAdJavaNameById() {
		return getAdJavaNameById;
	}

	public void setGetAdJavaNameById(String getAdJavaNameById) {
		this.getAdJavaNameById = getAdJavaNameById;
	}
}
