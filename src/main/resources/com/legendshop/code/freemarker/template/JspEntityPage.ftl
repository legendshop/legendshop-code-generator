<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
    <meta charset="utf-8">
    <title>创建${table.tableComment} - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="${table.tableComment}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="${r"${"}contextPath}/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" href="${r"${"}contextPath}/favicon.ico">
    <link rel="stylesheet" type="text/css" media="screen" href="${r"${"}contextPath}/resources/common/css/errorform.css" />
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${r"${"}contextPath}${actionPrex}/${entityClassInstance}/save" method="post" id="form1">
            <input id="id" name="id" value="${r"${"}bean.id}" type="hidden">
            <div align="center">
                <table class="${r"${"}tableclass}" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="no-bg title-border">首页管理&nbsp;＞&nbsp;
	                        <span style="color:#0e90d2; font-size:14px;"><a href="${r"${"}contextPath}${actionPrex}/${entityClassInstance}/query">${table.tableComment}</a></span>&nbsp;＞&nbsp;
	                        <span style="color:#0e90d2;">创建${table.tableComment}</span>
                        </th>
                    </tr>
                    </thead>
                </table>
                <table  align="center" class="${r"${"}tableclass} no-border" id="col1" style="width: 100%">
                
                <#list columns as column>
            	<#if !column.isPrimaryKeyColumn()>
            		<tr>
            		        <td width="15%">
            		          	<div align="right"><@upperFirstChar>${column.columnComment}</@upperFirstChar>: <font color="ff0000">*</font></div>
            		       	</td>
            		        <td align="left">
            		           	<input type="text" name="${column.javaName}" id="${column.javaName}" value="${r"${"}${entityClassInstance}.${column.javaName}}" />
            		        </td>
	            		</tr>
	                </#if> 
	            </#list>
                 <tr>
                        <td colspan="2">
                            <div align="center">
                                <input class="${r"${"}btnclass}" type="submit" value="保存" />
                                <input class="${r"${"}btnclass}" type="button" value="返回" onclick="window.location='${r"${"}contextPath}${actionPrex}/${entityClassInstance}/query'" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form:form>
    </div>
</div>
</body>
<script src="${r"${"}contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script language="javascript">
    jQuery.validator.setDefaults({});
    jQuery(document).ready(function() {
        jQuery("#form1").validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 50
                }
            },
            messages: {
                title: {
                    required: "请输入标题",
                    maxlength: "最大长度为50"
                }
            }
        });
    });
</script>
</html>
