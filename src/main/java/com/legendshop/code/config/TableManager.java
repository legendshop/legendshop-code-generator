package com.legendshop.code.config;


import java.sql.ResultSet;
import java.sql.SQLException;


public class TableManager
{

    public TableManager()
    {
    }

    public Table getTableInfo(String name)
    {
        return null;
    }
   //得到主键，有可能是复合主键
    protected String[] getKey(ResultSet rs)
        throws SQLException
    {
        String r[] = null;
        String indexName = "";
        int i;
        for(i = 0; rs.next(); i++);//i为rs的大小
        r = new String[i];
        while(rs.previous()) 
            r[--i] = rs.getString("column_name").toLowerCase();
        return r;
    }
    //主键在所有字段的位置
    protected int[] getKeyPos(String cs[], String uniques[])
    {
        if(cs == null || uniques == null)
            return new int[0];
        int pos[] = new int[uniques.length];
        for(int i = 0; i < uniques.length; i++)
        {
            for(int j = 0; j < cs.length; j++)
            {
                if(!cs[j].equals(uniques[i]))
                    continue;
                pos[i] = j;
                break;
            }

        }

        return pos;
    }

    public String[] getAllTableName()
    {
        return null;
    }
}

