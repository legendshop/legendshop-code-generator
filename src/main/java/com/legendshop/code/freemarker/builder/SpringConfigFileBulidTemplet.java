package com.legendshop.code.freemarker.builder;

import java.util.Map;


import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.Templet;

@Templet
public class SpringConfigFileBulidTemplet extends AbstractBulidTemplet {

	public SpringConfigFileBulidTemplet(){
		
	}
	
	private static int counter = 0;
	@Override
	public boolean buildSourceFile(String tableName) {
		//System.out.println("Start Building SpringConfigFile File.");
		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.ROOT + PackageConfigUtil.getSpringConfigPackagePath();
		String templateFileName = "SpringConf.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);

		String fileName = "applicationContext.xml";
		counter ++;
		boolean overWritten = false;
		if(counter > 1){
			overWritten = true;
		}
		
		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("table", table);
		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,overWritten);

		//System.out.println("End Build SpringConfigFile File.\n");
		return result;
	}

}
