/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package ${daoPackagePath}.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import ${modelPackagePath}.${entityClass};
import ${daoPackagePath}.${daoClass};

/**
 * The Class ${daoClass}Impl. ${table.tableComment}Dao实现类
 */

public class ${daoClass}Impl extends GenericDaoImpl<${entityClass}, ${IdType}> implements ${daoClass}{

	/**
	 * 根据Id获取${table.tableComment}
	 */
	public ${entityClass} get${entityClass}(${IdType} id){
		return getById(id);
	}

	/**
	 *  删除${table.tableComment}
	 *  @param ${entityClassInstance} 实体类
	 *  @return 删除结果
	 */	
    public int delete${entityClass}(${entityClass} ${entityClassInstance}){
    	return delete(${entityClassInstance});
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int delete${entityClass}(${IdType} id){
		return deleteById(id);
	}

	/**
	 * 保存${table.tableComment}
	 */
	public ${IdType} save${entityClass}(${entityClass} ${entityClassInstance}){
		return save(${entityClassInstance});
	}

	/**
	 *  更新${table.tableComment}
	 */		
	public int update${entityClass}(${entityClass} ${entityClassInstance}){
		return update(${entityClassInstance});
	}

	/**
	 * 分页查询${table.tableComment}列表
	 */
	public PageSupport<${entityClass}>query${entityClass}(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(${entityClass}.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

}
