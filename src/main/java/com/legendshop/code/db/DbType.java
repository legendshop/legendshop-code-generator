package com.legendshop.code.db;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.unitils.core.dbsupport.DbSupport;
import org.unitils.core.dbsupport.DefaultSQLHandler;
import org.unitils.core.dbsupport.DerbyDbSupport;
import org.unitils.core.dbsupport.MsSqlDbSupport;
import org.unitils.core.dbsupport.MySqlDbSupport;
import org.unitils.core.dbsupport.OracleDbSupport;
import org.unitils.core.dbsupport.SQLHandler;

import com.legendshop.code.config.Config;
import com.legendshop.code.db.map.MySQLTypeMap;
import com.legendshop.code.db.map.OracleTypeMap;
import com.legendshop.code.db.map.TypeMap;
import com.legendshop.code.db.table.DatabaseSchemaHelper;
import com.legendshop.code.db.table.MysqlSchemaHelper;



public enum DbType{
	MYSQL() {
		@Override
		public DbSupport getDbSupport() {
			return new MySqlDbSupport(){
				public SQLHandler getSQLHandler() {
			        return new DefaultSQLHandler(dataSource);
			    }
				public String getSchemaName() {
					return Config.getInstance().getDbSchema();
			    }
			};
		}

		@Override
		public String getDbUnitDialect() {
			return "mysql";
		}

		@Override
		public DatabaseSchemaHelper getColumnSchema() {
			return new MysqlSchemaHelper();
		}
		@Override
		public TypeMap getTypeMap() {
			return new MySQLTypeMap();
		}
	},
	ORACLE() {
		@Override
		public DbSupport getDbSupport() {
			return new OracleDbSupport(){
				public SQLHandler getSQLHandler() {
			        return new DefaultSQLHandler(dataSource);
			    }
				public String getSchemaName() {
					return Config.getInstance().getDbSchema();
			    }
			};
		}

		@Override
		public String getDbUnitDialect() {
			return "oracle";
		}
		@Override
		public DatabaseSchemaHelper getColumnSchema() {
			return null;
		}
		@Override
		public TypeMap getTypeMap() {
			return new OracleTypeMap();
		}
	},
	SQLSERVER() {

		@Override
		public DbSupport getDbSupport() {
			return new MsSqlDbSupport(){
				public SQLHandler getSQLHandler() {
			        return new DefaultSQLHandler(dataSource);
			    }
				public String getSchemaName() {
			        return Config.getInstance().getDbSchema();
			    }
			};
		}

		@Override
		public String getDbUnitDialect() {
			return "mssql";
		}
		@Override
		public DatabaseSchemaHelper getColumnSchema() {
			return null;
		}
		@Override
		public TypeMap getTypeMap() {
			return new MySQLTypeMap();
		}
	},

	DERBYDB() {
		@Override
		public DbSupport getDbSupport() {
			return new DerbyDbSupport(){
				public SQLHandler getSQLHandler() {
			        return new DefaultSQLHandler(dataSource);
			    }
				public String getSchemaName() {
			        return Config.getInstance().getDbSchema();
			    }
			};
		}

		@Override
		public String getDbUnitDialect() {
			return "derby";
		}
		@Override
		public DatabaseSchemaHelper getColumnSchema() {
			return null;
		}
		@Override
		public TypeMap getTypeMap() {
			return new MySQLTypeMap();
		}

	},

	UNSUPPORT() {
		@Override
		public DbSupport getDbSupport() {
			throw new RuntimeException("unsupport database type");
		}

		@Override
		public String getDbUnitDialect() {
			throw new RuntimeException("unsupport database type");
		}
		@Override
		public DatabaseSchemaHelper getColumnSchema() {
			throw new RuntimeException("unsupport database type");
		}
		@Override
		public TypeMap getTypeMap() {
			throw new RuntimeException("unsupport database type");
		}
	};
	private DbType(){
		
	}
	
	private DbType(String driver,String url,String user,String passwd){
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.passwd = passwd;
	}
	
	private static DbType instance = null;
	
	public static DbType getInstance(){
		if(instance != null){
			return instance;
		}
		String type = Config.getInstance().getDbType();;
		for(DbType dbType:DbType.values()){
			if(dbType.getDbUnitDialect().equalsIgnoreCase(type)){
				return dbType;
			}
		}
		return DbType.UNSUPPORT;
	}
	
	
	private String driver;
	private String url;
	private String user;
	private String passwd;
	private static BasicDataSource dataSource;
	
	public static BasicDataSource getDataSource() {
		return dataSource;
	}

	static{
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(Config.getInstance().getDbDriver());
		dataSource.setUrl(Config.getInstance().getDbConnectString());
		dataSource.setUsername(Config.getInstance().getDbUsername());
		dataSource.setPassword(Config.getInstance().getDbPasswd());
	}
	
	public String getDriver() {
		return StringUtils.isEmpty(driver) ? Config.getInstance().getDbDriver() : this.driver;
	}
	public String getUrl() {
		return StringUtils.isEmpty(url) ? Config.getInstance().getDbDriver() : this.url;
	}
	public String getUser() {
		return StringUtils.isEmpty(user) ? Config.getInstance().getDbUsername() : this.user;
	}
	public String getPasswd() {
		return StringUtils.isEmpty(passwd) ? Config.getInstance().getDbPasswd() : this.passwd;
	}
	public abstract String getDbUnitDialect();

	public abstract DbSupport getDbSupport();
	
	public abstract DatabaseSchemaHelper getColumnSchema();
	
	public abstract TypeMap getTypeMap();
	
}
