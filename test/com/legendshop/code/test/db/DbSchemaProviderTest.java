package com.legendshop.code.test.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.legendshop.code.db.Column;
import com.legendshop.code.db.DbSchemaProvider;

public class DbSchemaProviderTest extends TestCase{
	@Before
	public void setUp(){
		
	}
	@After
	public void tearDown(){
		
	}
	@Test
	public void testIsTablesExsit(){
		Set<String> tablesName = new HashSet<String>();
		tablesName.add("ls_prod_spec");
		assertEquals(true,DbSchemaProvider.isTablesExsit(tablesName));
		/*tablesName.add("app1");
		assertEquals(false,DbSchemaProvider.isTablesExsit(tablesName));*/
	}
	@Test
	public void testGetTableColumnNames(){
		List<Column> list = DbSchemaProvider.getTableColumnNames("ls_prod_spec");
		for(Column column : list){
			System.out.println(column.toString());
		}
	}
}
