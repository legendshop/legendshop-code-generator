package com.legendshop.code.test.util;

import java.util.HashMap;

import org.apache.oro.text.regex.MalformedPatternException;

import com.legendshop.code.util.StringConverter;

public class OROTest {

    public static void main(String[] args) throws MalformedPatternException {
        HashMap map = new HashMap();
        map.put("#daoPackagePath#", "com.mxidea.bizservice.dao");
        map.put("#daoName#", "BaseDao");

        String pattern = "\\#[a-zA-Z]+\\#";
        String text = "public class #daoName# extends BaseDao {";
        String result = StringConverter.convert(text, pattern, map);
        System.out.println(result);
        //String source=StringConverter.convertTemplate("Dao.template", pattern, map);
        //System.out.println(source);
        // Log.writeFile(source, "c:/test1/test.java");
    }
}
