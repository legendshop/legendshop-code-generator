package com.legendshop.code.db;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.map.TypeMap;
import com.legendshop.code.db.table.DatabaseSchemaHelper;
import org.unitils.core.dbsupport.DbSupport;

/**
 * 
 * @author longhao
 * @since
 * @version
 * 
 */
public class DbSchemaProvider {
	private static DbSupport dbSupport = DbType.getInstance().getDbSupport();
	private static DatabaseSchemaHelper columnSchema = DbType.getInstance().getColumnSchema();
	private static TypeMap typeMap = DbType.getInstance().getTypeMap();

	public static boolean isTablesExsit(Set<String> tablesName) {
		Set<String> set = dbSupport.getTableNames();
		return CollectionUtils.isSubCollection(tablesName, set);
	}

	public static List<Column> getTableColumnNames(String tableName) {
		String schema = Config.getInstance().getDbSchema();
		List<Column> list = columnSchema.getTableColumns(schema, tableName);
		for (Column column : list) {
			column.setJavaName(convertJavaName(column.getColumnName()));
			String dataType= column.getDataType();
			if(dataType == null){
				throw new RuntimeException("error in Column: " + column);
			}
			Class<?> clazz = typeMap.javaTypeMap().get(dataType.toUpperCase());
			
			if(clazz == null){
				throw new RuntimeException("error in dataType: " + dataType + ", Column: " + column);
			}
			String javaType = clazz.getName();

			if (("TINYINT".equals(column.getDataType().toUpperCase()))) {
				//if((1 == column.getNumericPrecision()))
					javaType = "java.lang.Boolean";
			}

			column.setJavaType(javaType);
			String javaSimpleType = javaType;
			if (javaSimpleType != null && javaSimpleType.contains("java.lang.")) {
				javaSimpleType = javaSimpleType.replace("java.lang.", "");
			}

			if (javaSimpleType != null && javaSimpleType.contains("java.util.")) {
				javaSimpleType = javaSimpleType.replace("java.util.", "");
			}
			column.setJavaSimpleType(javaSimpleType);
			Object value = column.getDefaultValue();
			if (value == null) {
				if ("String".equals(javaSimpleType)) {
					value = "";
				}
				if ("Integer".equals(javaSimpleType)) {
					value = 0;
				}
				if ("Long".equals(javaSimpleType)) {
					value = 0L;
				}
				if ("java.util.Date".equals(javaSimpleType)) {
					value = new java.util.Date();
				}
			}
		}
		return list;
	}

	public static Table getTableDefinition(String tableName) {
		String schema = Config.getInstance().getDbSchema();
		Table result = columnSchema.getTableSimpleDefinition(schema, tableName);
		String convertJavaName = convertJavaName(tableName);
		result.setTableJavaName(convertJavaName);

		List<Column> columnList = getTableColumnNames(tableName);
		result.setColumnList(columnList);
		return result;
	}

	public static String convertJavaName(String name) {
		name = name.toLowerCase();
		int index = name.indexOf("_");
		while (index != -1) {
			name = name.substring(0, index) + Character.toUpperCase(name.charAt(index + 1)) + name.substring(index + 2);
			index = name.indexOf("_");
		}

		return name;
	}

	public static String convertTableName(String tableName) {
		tableName = convertJavaName(tableName);
		return tableName.substring(0, 1).toUpperCase() + tableName.substring(1);
	}
	
	public static void main(String[] args) {
		System.out.println(convertJavaName("user_name_id"));
		System.out.println(convertJavaName("userNameID"));
		System.out.println(convertTableName("user_name_id"));
		System.out.println(convertTableName("userNameID"));
	}

}
