package com.legendshop.code.freemarker.builder;

import java.util.Map;


import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.Templet;

@Templet
public class DaoImplFileBulidTemplet extends AbstractBulidTemplet {

	public DaoImplFileBulidTemplet(){
		
	}
	@Override
	public boolean buildSourceFile(String tableName) {
		//System.out.println("Start Building DaoImpl File.");

		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.MAIN_PATH + PackageConfigUtil.getDaoImplPackagePath();
		String templateFileName = "daoImpl.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);

		String entityClass = (String) paramMap.get("entityClass");
		String fileName = entityClass + "DaoImpl.java";

		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("table", table);
		
		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,false);

		//System.out.println("End Build DAO File.\n");
		return result;
	}

}
