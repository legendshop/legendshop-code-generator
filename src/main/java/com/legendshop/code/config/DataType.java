package com.legendshop.code.config;

import java.util.HashMap;

public class DataType {
    public static DataType _instance = new DataType();
    private final HashMap typeMap;
    public static final int DATA_INT = 0;
    public static final int DATA_FLOAT = 1;
    public static final int DATA_String = 2;
    public static final int DATA_TIME = 3;
    public static final int DATA_SHORT = 4;
    public static final int DATA_LONG = 5;
    //    public static final String DATA_TYPE[] = {
    //        "int", "double", "String", "Timestamp"
    //    };
    public static final String DATA_TYPE[] = { "Integer", "Double", "String", "Date", "Short", "Long" };
    public static final String DATA_TYPE_CLASS[] = { "java.lang.Integer", "java.lang.Double", "java.lang.String",
            "java.util.Date", "java.lang.Short", "java.lang.Long" };

    private DataType() {
        typeMap = new HashMap();
        typeMap.put("number", new Integer(DATA_FLOAT));
        typeMap.put("tinyint", new Integer(DATA_SHORT));
        typeMap.put("smallint", new Integer(DATA_SHORT));
        typeMap.put("int", new Integer(DATA_LONG)); //int
        typeMap.put("int unsigned", new Integer(DATA_LONG)); //int
        typeMap.put("bigint", new Integer(DATA_LONG));
        typeMap.put("numeric", new Integer(DATA_FLOAT));
        typeMap.put("bit", new Integer(DATA_SHORT));
        typeMap.put("numberf", new Integer(DATA_FLOAT));
        typeMap.put("money", new Integer(DATA_FLOAT));
        typeMap.put("float", new Integer(DATA_FLOAT));
        typeMap.put("decimal", new Integer(DATA_FLOAT));
        typeMap.put("decimalf", new Integer(DATA_FLOAT));
        typeMap.put("smallmoney", new Integer(DATA_FLOAT));

        typeMap.put("varchar2", new Integer(DATA_String));
        typeMap.put("text", new Integer(DATA_String));
        typeMap.put("ntext", new Integer(DATA_String));
        typeMap.put("varbinary", new Integer(DATA_String));
        typeMap.put("varchar", new Integer(DATA_String));
        typeMap.put("binary", new Integer(DATA_String));
        typeMap.put("char", new Integer(DATA_String));
        typeMap.put("nvarchar", new Integer(DATA_String));
        typeMap.put("nchar", new Integer(DATA_String));
        typeMap.put("ntext", new Integer(DATA_String));

        typeMap.put("date", new Integer(DATA_TIME));
        typeMap.put("smalldatetime", new Integer(DATA_TIME));
        typeMap.put("datetime", new Integer(DATA_TIME));
        typeMap.put("timestamp", new Integer(DATA_TIME));
        typeMap.put("time", new Integer(DATA_TIME));
    }

    public static DataType getInstance() {
        return _instance;
    }

    public static String getDescription(int dataType) {
        if ((dataType >= 3) || (dataType < 0)) {
            return "";
        } else {
            return DATA_TYPE[dataType];
        }
    }

    public int getTypeMap(String type) {
        Integer r = (Integer) typeMap.get(type);
        if (r != null) {
            return r.intValue();
        } else {
            return 2;
        }
    }

}
