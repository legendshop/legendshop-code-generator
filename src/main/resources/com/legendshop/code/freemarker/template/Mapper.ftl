package ${modelPackagePath} ;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
	
public class ${entityClass}Mapper implements RowMapper<${entityClass}> {
		public ${entityClass} mapRow(ResultSet rs, int rowNum) throws SQLException {
		   ${entityClass} ${entityClassInstance} = new ${entityClass}();
		  <#list columns as column>
		  	${entityClassInstance}.set<@upperFirstChar>${column.javaName}</@upperFirstChar>(rs.get${column.javaSimpleType}("${column.columnName}"));
		  </#list>
		  return ${entityClassInstance};
		}

}