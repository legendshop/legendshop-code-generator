
    
    <bean id="${daoClassInstance}" class="${daoPackagePath}.impl.${daoClass}Impl" parent="genericDao" />

	<bean id="${entityClassInstance}Service" class="${servicePackagePath}.impl.${entityClass}ServiceImpl">
        <property name="${daoClassInstance}" ref="${daoClassInstance}" />
    </bean>  
