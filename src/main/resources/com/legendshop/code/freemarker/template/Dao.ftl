/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package ${daoPackagePath};

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import ${modelPackagePath}.${entityClass};

/**
 * The Class ${daoClass}. ${table.tableComment}Dao接口
 */
public interface ${daoClass} extends Dao<${entityClass},${IdType}>{

	/**
	 * 根据Id获取${table.tableComment}
	 */
	public abstract ${entityClass} get${entityClass}(${IdType} id);

	/**
	 *  根据Id删除${table.tableComment}
	 */
    public abstract int delete${entityClass}(${IdType} id);

	/**
	 *  根据对象删除
	 */
    public abstract int delete${entityClass}(${entityClass} ${entityClassInstance});

	/**
	 * 保存${table.tableComment}
	 */
	public abstract ${IdType} save${entityClass}(${entityClass} ${entityClassInstance});

	/**
	 *  更新${table.tableComment}
	 */		
	public abstract int update${entityClass}(${entityClass} ${entityClassInstance});

	/**
	 * 分页查询${table.tableComment}列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<${entityClass}> query${entityClass}(String curPageNO, Integer pageSize);

}
