package com.legendshop.code.test.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;


public class TestGetIndex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			DatabaseMetaData databaseMetaData = conn.getMetaData();
			System.out.println(databaseMetaData.getUserName());
			ResultSet indexRet=databaseMetaData.getIndexInfo(null, "%", "t_user", false, false);
        	while(indexRet.next()) {
            	String index_name = indexRet.getString("index_name");
            	String column_name = indexRet.getString("column_name");
            	String pages = indexRet.getString("PAGES");
            	String type = indexRet.getString("NON_UNIQUE");
            	String tablename = indexRet.getString("TABLE_NAME");
            	//int TYPE_NAME = indexRet.getInt("TYPE_NAME");
            	//int digits = indexRet.getInt("DECIMAL_DIGITS");
            	//int nullable = indexRet.getInt("data_scale"); 
           	System.out.println(index_name+" "+column_name + " "+pages+" "+type+" " +tablename);
            	}
			System.out.println(databaseMetaData.getIndexInfo(null, "%", "t_user", true, true));
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
