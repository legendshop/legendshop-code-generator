package com.legendshop.code.freemarker.builder;

import java.util.Map;


import com.legendshop.code.config.Config;
import com.legendshop.code.config.Table;
import com.legendshop.code.db.DbSchemaProvider;
import com.legendshop.code.freemarker.AbstractBulidTemplet;
import com.legendshop.code.freemarker.BulidTemplet;
import com.legendshop.code.freemarker.PackageConfigUtil;
import com.legendshop.code.util.Templet;

@Templet
public class EntityFileBulidTemplet extends AbstractBulidTemplet {

	public EntityFileBulidTemplet(){
		
	}
	
	@Override
	public boolean buildSourceFile(String tableName) {
		//System.out.println("Start Building Entity File.");

		Config config = Config.getInstance();
		String outputDir = config.getPath() + BulidTemplet.MAIN_PATH + PackageConfigUtil.getModelPackagePath();
		String templateFileName = "Entity.ftl";

		Map<String, Object> paramMap = config.getContextMap(tableName);
		Table table = DbSchemaProvider.getTableDefinition(tableName);
		paramMap.put("columns", table.getColumnList());
		paramMap.put("table", table);
		paramMap.put("sequenceName", parseDefaultSequenceName(table.getTableName()));
		String entityClass = (String) paramMap.get("entityClass");

		String fileName = entityClass + ".java";

		boolean result = super.buildJavaFile(templateFileName, paramMap, outputDir, fileName,false);

		//System.out.println("End Build Entity File.\n");
		return result;
	}
	
	private String parseDefaultSequenceName(String tableName){
		String sequenceName = tableName.toUpperCase() + "_SEQ";
		if(sequenceName.startsWith("LS_")){
			sequenceName = sequenceName.substring(3, sequenceName.length());
		}
		
		return sequenceName;
	}

}
