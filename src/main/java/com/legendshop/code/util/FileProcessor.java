package com.legendshop.code.util;

import java.io.File;

public class FileProcessor {
	/**
	 * 删除目录和下面的所有的文件.
	 * 
	 * @param path
	 *            the path
	 */
	public static void deleteDirectory(File path) {
		if (!path.exists())
			return;
		if (path.isFile()) {
			path.delete();
			return;
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteDirectory(files[i]);
		}
		path.delete();
	}
	
	/**
	 * 删除文件.
	 * 
	 * @param fileName
	 *            文件名称
	 * @param backup 是否备份大图片
	 * @return
	 *  0： 删除成功，
	 * -1：文件存在，但删除失败
	 * 1： 没有文件
	*	2： 未知原因失败
	 */
	public static int deleteFile(String filename) {
		try {
			File f = new File(filename);
			if (f.exists()) {// 检查是否存在
				boolean result = f.delete();// 删除文件
				if(result){
					return 0; // 删除成功
				}else{
					return -1; // 删除失败
				}
				
			} else {
				System.out.println("没有该文件：" + filename);
				return 1;// 没有文件
			}
		} catch (Exception e) {
			System.out.println("删除文件 {} 失败" +  filename);
			return 2;// 失败
		}
	}

}
